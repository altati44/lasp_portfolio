"use client";

import React from "react";
import { store } from "./store";
import { Provider } from "react-redux";

interface Props { children: React.ReactNode }

// export function Providers({ children }: Props) {
export function Providers({ children }: { children: React.ReactNode }) {
  return <Provider store={store}>
    {children}
  </Provider>;
}