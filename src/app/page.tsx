"use client";

import { decrement, increment, reset } from "@/redux/features/counterSlice";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { useGetUsersQuery } from "@/redux/services/userApi";

function Home() {
  const count = useAppSelector((state) => state.counterReducer.value);
  const dispatch = useAppDispatch();

  const { isLoading, isFetching, data, error } = useGetUsersQuery(null);

  if (isLoading || isFetching) return <p>loading...</p>;
  if (error) return <p>some error</p>;

  // console.log(data);

  return (
    <>
      <div>
        <h4 style={{ marginBottom: 16 }}>{count}</h4>
        <button onClick={() => dispatch(increment())}>increment</button>
        <button
          onClick={() => dispatch(decrement())}
          style={{ marginInline: 16 }}
        >
          decrement
        </button>
        <button onClick={() => dispatch(reset())}>reset</button>
      </div>

      <div className="flex flex-row flex-wrap justify-center mx-auto gap-3">
        {
          data?.map(user => (
            <div key={user.id} className="bg-slate-400 p-4 rounded">
              <p className="text-center font-bold mt-4">{user.name}</p>
              <p className="text-center font-bold mt-4">{user.username}</p>
              <p className="text-center font-bold mt-4">{user.email}</p>
            </div>
          ))
        }
      </div>
    </>
  );
}

export default Home;